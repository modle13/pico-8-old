counters = {}

function create_counters()
    for i, v in pairs(counter_data) do
        -- 48 and 49 are the backing
        create_stationary(
            {48, 49, v.sprite_id},
            v.icon_position.x,
            v.icon_position.y,
            0,
            0,
            counters,
            i
        )
    end
end

function draw_counters()
    if (not state.show_counts) return
    -- counter sprites
    foreach(counters, draw_ui_sprite)
    -- counter counts
    for i, v in pairs(counter_data) do
        print(
            v.count,
            (v.count_position.x) * globals.cell_size + 0.1 * globals.cell_size,
            (v.count_position.y + 0.25) * globals.cell_size
        )
    end
end
