MAP_WIDTH = 16
MAP_HEIGHT = 16
cell_size = 8
room_x = 0
room_y = 0

msg='hello there'

function _init()
end

function _update()
end

function _draw()
    -- clear screen
    cls()

    map()

    --foreach(actor, draw_actor)
    draw_actor()
    handle_camera()

end

function draw_actor()
    spr(1, 5, 5)
    print(msg, 5, 20)
end

function handle_camera()
    camera(room_x * MAP_WIDTH * cell_size, room_y * MAP_HEIGHT * cell_size)
end
