function check_for_solid_actor(a)
    -- next position occupied?
    occupied_check_type='none'
    if (a.direction_y<0) occupied_check_type='up'
    if (a.direction_y>0) occupied_check_type='down'
    if (a.direction_x<0) occupied_check_type='left'
    if (a.direction_x>0) occupied_check_type='right'

    next_occupied=cell_occupied(a,occupied_check_type)!=nil
    if next_occupied then
        if a.name=='player' then
            if state.movement_mode=='player' and (btn(0) or btn(1) or btn(2) or btn(3)) then
                -- player is pressing movement keys
                -- so don't interfere
                return
            end
        end
        a.dx=0
        a.dy=0
    end
end

function next_is_solid(actor,dx,dy)
    local chx,chy=getvec(actor)
    if (actor.name=='player') chx=actor.rel_x chy=actor.rel_y
    return solid_area(chx+dx,chy+dy,actor.w,actor.h,'movement')
end

function solid_area(x,y,w,h,check_type,rm)
    return
        solid(x-w,y-h,check_type,rm) or
        solid(x+w,y-h,check_type,rm) or
        solid(x-w,y+h,check_type,rm) or
        solid(x+w,y+h,check_type,rm)
end

function solid(x,y,check_type,rm)
    if (check_type=='placement') return contains(state.solid_map_sprites[rm],prpos(x,y))
    return fget(mget(x,y),1)
end

function cell_occupied(actor1,area,offset_x,offset_y)
    if (actor1.name=='player') return
    if (offset_x==nil) offset_x=0
    if (offset_y==nil) offset_y=0
    -- offset has to be at least 1 for adjacent cell checking
    direction_offset=1
    if (area=='up' and offset_y==0) offset_y-=direction_offset
    if (area=='down' and offset_y==0) offset_y+=direction_offset
    if (area=='left' and offset_x==0) offset_x-=direction_offset
    if (area=='right' and offset_x==0) offset_x+=direction_offset
    check_pos=prpos(flr(actor1.x)+offset_x,flr(actor1.y)+offset_y)
    matched_actor=state.product_pos_map[check_pos]
    return matched_actor
end

function actor_collide(a1,a2,area,off_x,off_y)
    if (a1==a2) return false
    -- rect dims; a1=actor1
    a1x1=a1.x+off_x-a1.w
    a1x2=a1.x+off_x+a1.w
    a2x1=a2.x-a2.w
    a2x2=a2.x+a2.w
    a1y1=a1.y+off_y-a1.h
    a1y2=a1.y+off_y+a1.h
    a2y1=a2.y-a2.h
    a2y2=a2.y+a2.h

    -- extend coll. rect size by bubble dims
    if area=='bubble' then
        bubble_range=0.2
        a1x1-=bubble_range
        a1x2+=bubble_range
        a1y1-=bubble_range
        a1y2+=bubble_range
    end
    -- horiz & vert min/max
    collision=intersect(a1x1,a1x2,a2x1,a2x2) and intersect(a1y1,a1y2,a2y1,a2y2)
    return collision
end

function intersect(min1, max1, min2, max2)
  return max(min1, max1) > min(min2, max2) and
         min(min1, max1) < max(min2, max2)
end

function in_quad(actor,collection)
    -- check for coordinates occupied by objects in target collection relative to current sprite
    local x,y=getvec(actor)
    x=flr(x)
    y=flr(y)
    return collection[prpos(x, y)]
end

function calculate_rect_overlap(actor, target)
    x_overlap = max(0, min(target.x + 1, actor.x + 1) - max(actor.x, target.x))
    y_overlap = max(0, min(target.y + 1, actor.y + 1) - max(actor.y, target.y))
    return x_overlap, y_overlap
end
