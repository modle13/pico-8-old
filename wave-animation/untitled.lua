frame = 0
frames = 2
shift_map = false

function _update()
    frame += 0.1
    frame %= frames
    if flr(frame) == 0 then
        shift_map = false
    end
    if flr(frame) == 1 then
        shift_map = true
    end
end

function _draw()
    cls()

    -- map API
    -- map( celx, cely, sx, sy, celw, celh, [layer] )
    -- celx, cely: start position on cel map to draw
    -- sx  , xy  : x and y coords of screen to place in topleft corner
    -- celw, celh: are the number of map cells wide and high to draw
    if shift_map then
        map(1, 0, 0, 0, 16, 16)
    else
        map(0, 0, 0, 0, 16, 16)
    end
end
