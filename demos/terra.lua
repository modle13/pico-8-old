--terra - a terraria demake
--by cubee 🐱

function _init()

-- clear user memory
for i=-32768,0x55ff do
 poke(i)
end
reload()

-- title logo
tospr("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeaeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee9eeeeee8aeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeaeeeeeeae9eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee88eae88eee9a8eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee88aa988a9a9a9aa9aeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee8a9aaaaaaaaa9aaaaaaaeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee9a229aa929929929a98eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee8aa233294444443339aaeeeeeeee89eeeeeeeeeeeae9eeeeeeeeeeeaaeeeeeeeea944343444433342aa988eeeeeeeaee9e9eeeeee9aeeeeeeeeeeee9e9eeeeee8a9333343333333339aaaeea8eeee9e8e9eeeeeee898ee8eeeeaeee8eaeeeeee8a233333343332232aaa889e8ee8a99a8a898e88a89aea988ee89e88899eeeeeea92929243329aa99aaaa9a9988ea9a9aaaa999a9a9aaaaa8e8eae89a9a9aeeee8aaa9a934439aaaaaaaaaaa9aa9a9aa999aaaaaaaaa999aaa9888a999aa88eeee9aaaa243639aaaa996119aaaaa92222439aaaa929226b9aaaa9a934229a9eeeeeaaaa23762aaaa91777b79aaa9343344432aa9343344771aaaa93444429a8eeee8aa9336139aa96b7617769aa93443233439a93443333679aaa23223432a9eeeeaaaa267139aa97769a9661aa23432992342a24332923161aa933aa9342a98eee8aa936732aa96b19aaa1329a93349aa9333993439aa93169aa22aaa243999eeee9aa371339aa1771111743aa93439aa9239a94339aa9239aaaaaaa9233998eee8a9a2673399ab16b671622aa93349aaaaaaa93439aaaaaaaa999992333998eeee9a936b32aa9776199a99aaaa2332aaaaaaa93332aaaaaaaa233334332a9eeeee8aa21732aa93449aaaaaaaa93432aaaaaaa933329aaaaaa93333333339a9eeee9a933139aaa93329a99329aa2339aaaaaaaa2339aaa9aa92339aa9332a98eeee892333329aaa2433223329a933329aa9aaa933329a9aaaa2333223333a9eeeee8a933339aaaa93333332aaa93339aaaaaaa93339aaa9aaa992233333399eeeee99233339a9a9a923232aaaa93229a9999a993229a9a99a99aaa922333999eeee8a29929a98889aa9a9aa99aa99a998e8e98a99aa9898998e989aa9992a98eeeee899aa998eeee99aaaa88e899899eeeeeee99898eeeeeeeeee889a9aaa98eeeeee8998eeeeeeeee8e8eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee899a98eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee8eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",64,64,0)

t,worldtime,phurt,
palettes,
recipes,
toolstat,
drops=
0,6000,0,
split([[
130,132,4,
9,10,5,134,
131,3,139,7,
1,140,12,136;

128,130,132,
4,137,133,5,
1,131,3,6,
129,1,140,2;

0,128,130,
132,4,130,133,
129,1,131,13,
0,129,1,130;

0,0,128,
128,132,128,130,
0,129,1,5,
0,0,129,128;

0,0,0,
0,128,0,128,
0,0,129,130,
0,0,0,0;
]],";"),
split([[
17:3|5,59|torch;
68:1|17,5:4|campfire;
14:2|5|platform;
5|14:2|wood from platform;
78:1:15|5:6|door;
13:1:16|3:2|grey brick;
64:1:16|19:2|red brick;
75:1:15|5:5|table;

87:1:15|86:2,93|potion;
96:1:18|89:10,55:2|hermes boots;

74:1:4|55:1,61:2,52:99|endless quiver;
80:15|17:1,52:15|fire arrows;
52:25:15|3:1,5:1|arrows;

73:1:4|55:1,61:2,51:99|endless pouch;
81:20:18|57:1|silver bullets;
51:25:18|56:1|bullets;

92:1:4|57:2,59:99|endless gel pack;

15|5:10|workbench;
16:1:15|5:4,3:10,17:3|furnace;
18:1:15|56:5|anvil;
4:1:18|58:4,27:1|cursed forge;

69:4:15|5|wood wall;
66:4:15|2|dirt wall;
67:4:15|3|stone wall;
71:4:15|7|mud wall;
76:4:15|12|ebonwall;

5:1:15|69:4|wood;
2:1:15|66:4|dirt;
3:1:15|67:4|stone;
7:1:15|71:4|mud;
12:1:15|76:4|ebonstone;

1:4|2:4,89|grass;
6:4|7:4,89|jungle;
10:4|2:4,61|corrupt;

91:1:4|53,90|terra blade;
65:1:4|85,90|dogblaster;

53:1:4|24,84,34|nights edge;
8:1:4|72,55:2|flamethrower;
94:1:4|93:10,95|beetler;

84:1:18|60:10,89:10|blade of grass;
85:1:18|50,55:2|minishark;
95:1:18|48,55:1|corrupt repeater;

55:1:16|23:3|demonite bar;
24:1:18|55:3|lights bane;
26:1:18|55:4|nightmare pick;
72:1:15|17:4,5:6|wand of sparking;

58:1:16|22:4|gold bar;
34:1:18|58:2|sword;
28:1:18|58:3|bow;
33:1:18|58:3,5:4|pick;
35:1:18|58:2,5:3|axe;
32:1:18|58:2,5:3|hammer;

57:1:16|21:4|silver bar;
38:1:18|57:2|sword;
29:1:18|57:3|bow;
37:1:18|57:3,5:4|pick;
39:1:18|57:2,5:3|axe;36:1:18|57:2,5:3|hammer;

56:1:16|20:3|iron bar;
42:1:18|56:2|sword;
30:1:18|56:3|bow;
41:1:18|56:3,5:3|pick;
43:1:18|56:2,5:3|axe;
40:1:18|56:2,5:3|hammer;

46:1:15|5:2|sword;
31:1:15|5:3|bow;
45:1:15|5:3|pick;
47:1:15|5:3|axe;
44:1:15|5:3|hammer;

62:1:15|54:6|sus eye;
63:1:18|59:15|slime crown;
25:1:18|61:6,55:1|secret boss shhh;
88:1:4|89:6|the plant's treasured strawberry
]],";"),
split[[
0:block,
0:block,
0:block,
0:block,
0:block,
0:block,
0:block,
20:ranged:12:gel:1,
20:hpup:1:1,
0:block,
0:block,
0:block,
0:block,
0:block,
0:block,

0:block,
0:block,
0:block,
0:block,
0:block,
0:block,
0:block,
0:block,
18:melee:20,
0:summon:16:marco:22,
9:pick:15,
0,
11:ranged:26:arrow,
9:ranged:27:arrow,
8:ranged:28:arrow,
4:ranged:30:arrow,

9:hammer:28,
6:pick:17,
13:melee:20,
7:axe:26,
9:hammer:29,
6:pick:19,
11:melee:21,
6:axe:26,
7:hammer:30,
5:pick:20,
10:melee:21,
5:axe:27,
2:hammer:37,
4:pick:23,
7:melee:25,
3:axe:30,

8:ranged:20:arrow,
0:aight imma head out,
24:ranged:48:bullet,
7:bullet:109:8,
4:arrow:52:3,
42:melee:21:0:1,
0,0,0,0,0,
0:gel:123:3,
0,0,
0:summon:16:eoc:53,
0:summon:16:kingslime,

0:block,
30:ranged:10:bullet:1,
0:block,
0:block,
0:block,
0:block,
0,
0:block,
14:ranged:26:gel,
8:bullet:109:8:1,
6:arrow:52:3:1,
0:block,
0:block,
0,
0:block,
0,

7:arrow:80:3.5,
9:bullet:109:9,
0,0,
28:melee:30,
6:ranged:9:bullet:1,
15:hpup:60,
50:hpup:140,
0:summon:16:plantera:31,
0,0,
60:melee:18:0:1,
0:gel:123:4.5:1,
0,
20:ranged:18:arrow:1,
12:ranged:25:arrow:1,
0:boot,
]],
split[[
2,
2,
3,
4,
5,
7,
7,
8,
9,
2,
2
]]

-- what is this mess haha
pflip,pjump,pair,pdigt,pfall,pdiet,pswing,pangle,
mobs,lights,projectiles,craftable,items,
hoveritem,invsel,invopen,invside,craftsel,
mode,orbsmash,nextmusic,currentmusic,worldmusic,
swingitem,drops[79],drops[82],drops[83],tilenames
=
false,true,true,0,0,0,0,0,
{},{},{},{},{},
false,1,false,1,1,
1,0,0,-1,0,
{id=0,usetime=10},

-- oh no it got worse
78,5,5,
split[[forest grass
,dirt
,stone
,cursed forge
,wood
,jungle grass
,mud
,flamethrower
,life crystal
,corrupt grass
,
,ebonstone
,grey brick
,wooden platform
,workbench
,furnace
,torch
,anvil
,clay
,iron ore
,silver ore
,gold ore
,demonite ore
,light's bane
,sigil of blight
,nightmare pickaxe
,soul of blight
,gold bow
,silver bow
,iron bow
,wood bow
,gold hammer
,gold pickaxe
,gold broadsword
,gold axe
,silver hammer
,silver pickaxe
,silver broadsword
,silver axe
,iron hammer
,iron pickaxe
,iron broadsword
,iron axe
,wooden hammer
,wooden pickaxe
,wooden broadsword
,wooden axe
,shade bow
,magic mirror
,musket
,musket ball
,wood arrow
,night's edge
,lens
,demonite bar
,iron bar
,silver bar
,gold bar
,gel
,stinger
,rotten chunk
,suspicious-looking eye
,slime crown
,red brick
,cubee's dogblaster
,dirt wall
,stone wall
,campfire
,wood wall
,heart
,mud wall
,wand of sparking
,endless musket pouch
,endless quiver
,table
,ebonstone wall
,shadow orb
,wooden door
,
,flaming arrow
,silver bullet
,
,
,blade of grass
,minishark
,mushroom
,healing potion
,plantera bulb
,leafy seed
,golden seed
,terra blade
,endless gel pack
,beetle husk
,beetle bow
,demonite repeater
,hermes boots
]]

-- generate
generate()

loadplayer"100,100|46:1,45:1,47:1"
spawnplayer()

for k,i in pairs(recipes) do
 recipes[k]=split(i,"|")
 recipes[k][1],recipes[k][2]=
 split(recipes[k][1],":"),split(recipes[k][2])
 for j,o in pairs(recipes[k][2]) do
  recipes[k][2][j]=split(o,":")
 end
end

if stat(6)~="" then
loadplayer(stat(6))
mode=2
end

music(0)

end

function _update60()
-- set btnp repeat
poke(0x5f5c,invopen and 0 or -1)

pgfx=pgfx or ""

smolmap()

if mode<3 then

if stat(120) then

 -- load world
 if mode==2 then
  for i=0,serial(0x800,0x8000,32767)-1 do
   poke(0x8000+i,@(0x8000+i)-32)
  end
  spawnplayer(php)

 -- load player
 else
  serial(0x800,0x4300,4864)
  s=""
  for i=0,4864 do
   s..=chr(@(0x4300+i))
  end
  loadplayer(s)
 end
 loadedfile=true
end

if(btnp(5))load("#terra_char_edit")

if btnp(4) or loadedfile then
 mode+=1
 loadedfile=false
 sfx(7)
end

-- put the normal gfx back
if mode>2 then
 reload()
end

-- game
else

mx=1

-- save button
menuitem(1,"save",function()
sp,sw=php..","..php_max..","..phurt.."|",""

genprint"processing player..."
for k,i in pairs(inventory) do
sp..=i.id..":"..i.amount..(k<#inventory and "," or "|")
end

genprint"processing world..."
for i=0,32767 do
sw..=chr(@(i+0x8000)+32)
end

printh(sp..pgfx.."|","@clip",true)

repeat
genprint("player copied to clipboard","press pause to continue")
until btn(6)

printh(sw,"@clip",true)

repeat
genprint("world copied to clipboard","press pause to continue")
until btn(6)
end)

bigmap()

-- clean inventory, update item stats
for i in all(inventory) do
stats=split(toolstat[i.id],":") or split"0,item"

local itype=i.type
i.type=itype or stats[2] or "item"

itool=itype=="pick" or itype=="axe" or itype=="hammer"

i.damage,i.var,i.usetime,
i.amount,i.autoswing,
i.melee=
stats[1],stats[4],stats[3] or 16,
i.amount or 1,stats[5] or itool or itype=="block",
itype=="melee" or itool

if(itype=="boot")mx=1.4

if i.amount<=0 then
 del(inventory,i)
end
end
if(invsel>#inventory)invsel=#inventory

-- inventory
if invopen then

-- inventory side
if invside==1 then
 if(btnp(0) and #craftable>0)invside=0
 if(btnp(2))invsel-=1
 if(btnp(3))invsel+=1
 if(invsel<1)invsel=#inventory
 if(invsel>#inventory)invsel=1

 -- moving items (place back if leaving inventory)
 if btnp(4) or ((btnp(6) or btnp(0) or btnp(1)) and hoveritem) then
  if not hoveritem then
   hoveritem=deli(inventory,invsel)
  else
   add(inventory,hoveritem,#inventory>0 and invsel or 1)
   hoveritem=false
  end

 end

 -- trash selected item
 if btnp(5) then
  trashslot=deli(inventory,invsel)
  updaterecipes()
 end

 -- recover trashed item
 if btnp(1) and trashslot then
  add(inventory,trashslot)
  trashslot=false
  updaterecipes()
 end

 -- stay at bottom of inventory
 if(invsel>#inventory)invsel=#inventory

-- craft side
else
 if(btnp(1))invside=1
 if(btnp(2))craftsel-=1
 if(btnp(3))craftsel+=1
 if(craftsel<1)craftsel=#craftable
 if(craftsel>#craftable)craftsel=1

 local it=craftable[craftsel]
 if it then
  local out,ing=it[1],it[2]

  -- craft
  if btnp(4) then

   -- check if ingredients available
   avail=false
   for i in all(ing) do
    for a=1,#inventory do
     if inventory[a].id==i[1] then
      avail=inventory[a].amount>=(i[2] or 1)
     end
    end
   end

   if avail then

       -- use ingredients
       for i in all(ing) do
        local amount=i[2] or 1

     -- search inventory
     for b=1,#inventory do

      -- remove if found
      if inventory[b].id==i[1] then
       inventory[b].amount-=amount
      end
     end
       end
   
       -- search inventory for this item
   
       local ininv,slot=false,1
       for a in all(inventory) do
        if a.id==out[1] then
         ininv=true
        end
        if(not ininv)slot+=1
       end
       if ininv then
        inventory[slot].amount+=out[2] or 1
       else
        add(inventory,{id=out[1],amount=out[2] or 1})
       end

   end

  end

  -- refresh recipe list
  if(pbtn4 and not btn(4))updaterecipes()
  pbtn4=btn(4)

 else
  invside=1
 end

end

pjump=true

-- gameplay
else

lights,bossmusic,invside,craftsel={},false,1,1

-- time, slightly faster than 1 min = 1 hour
worldtime+=0.4
worldtime%=24000

-- player dead
if pdead then
 pdiet=max(pdiet-1)

 if pdiet==0 and btnp(4) then
  spawnplayer(max(php_max/2,100))
 end

-- player alive
else

 -- healing
 if(t%100==0)php+=1

 frc=pair and 0.03 or 0.08

 -- left
 if btn(0) then

  if(pxv>-mx)pxv-=pxv>0 and 0.15 or 0.08
  if(canturn)pflip=true

 -- right
 elseif btn(1) then

  if(pxv<mx)pxv+=pxv<0 and 0.15 or 0.08
  if(canturn)pflip=false

 else

  -- friction
  pxv-=sgn(pxv)*frc
  if abs(pxv)<=frc then
   pxv=0
  end
 end

 -- apply gravity
 pyv+=0.08

 -- collisions

 -- down
 d,e,pair=0,false,true
 repeat
  local y=(py+4+d)/8
  l,r=bget(px/8-0.375,y),bget(px/8+0.25,y)
  if pyv>0 and (fget(l,0) or fget(r,0) or (l==14 or r==14) and not btn(3)) then
   if(not btn(4))pjump=false
   pair,e,pyv=false,true,0
   py+=d

   -- fall damage
   pfall=max(pfall/8-13)
   if pfall>0 then
    php-=flr(pfall*10)
    piframes=30
    sfx(0,-1,phurt,10)
   end
   pfall=0

         if btn(4) and not pjump then
          pjump,pyv=true,-2.2
         end

  end
  d+=1
 until d>pyv or e

 -- up
 d,e=0,false
 repeat
  local y=(py-5+d)/8
  lef,rgt=fget(bget(px/8-0.375,y),0),fget(bget(px/8+0.25,y),0)
  if pyv<0 and (lef or rgt) then
   e=true
   if lef and rgt then
    pyv=0
    py+=d
   end
   if(not rgt)px+=1
   if(not lef)px-=1
  end
  d-=1
 until d<pyv or e

 -- right
 d,e=0,false
 repeat
  local bx,by=(px+3+d)/8,py/8+0.375
  local b,top=bget(bx,by),fget(bget(bx,py/8-0.375),0)

  -- doors
  if fget(b,5) then
   if pxv>0 and fget(b,0) then
    mset(bx,by,b+1)
   elseif pxv<0 and not fget(b,0) then
    mset(bx,by,b-1)
   end

  -- normal collision
  elseif pxv>0 and (top or fget(b,0)) then
   e,pxv=true,0
   px+=d
   if(not top)py-=1 pyv=min(pyv)
  end
  d+=1
 until d>pxv or e

 -- left
 d,e=0,false
 repeat
  local bx,by=(px-4+d)/8,py/8+0.375
  local b,top=bget(bx,by),fget(bget(bx,py/8-0.375),0)

  -- doors
  if fget(b,5) then
   if pxv<0 and fget(b,0) then
    mset(bx,by,b+1)
   elseif pxv>0 and not fget(b,0) then
    mset(bx,by,b-1)
   end

  -- normal collision
  elseif pxv<0 and (top or fget(b,0)) then
   e,pxv=true,0
   px+=d
   if(not top)py-=1 pyv=min(pyv)
  end
  d-=1
 until d<pxv or e

 if pjump and not btn(4) then
  pyv=max(pyv,-0.2)
 end

 -- move
 px+=pxv
 py+=pyv
 pfall+=pyv

 -- use position
 digx,digy=pflip and -7 or 7,(btn(3) and 7) or (btn(2) and -7) or 0
 if not (btn(0) or btn(1)) then
  if(btn(3) or btn(2))digx=0
 end
 actx,acty=(px+digx)/8,(py+digy)/8
 tile,helditem,canturn,digging=bget(actx,acty),inventory[invsel] or {id=0,usetime=10},true,false

 -- use item
 hitype,hiuse=helditem.type,helditem.usetime
 if btn(5) then

  if pswing==0 and (btnp(5) or helditem.autoswing) then
   pswing,swingitem=hiuse,helditem
  end

  -- breakable tiles
  if hitype=="axe" or hitype=="pick" then

   if til~=0 and((hitype=="axe" and tile==83) or (hitype=="pick" and fget(tile,1) and bget(actx,acty-1)~=83))then
    digging=true
    pdigt+=1
          if(pdigt%hiuse==1)sfx(4)

          -- break tile
          if pdigt>=hiuse*3 then
           pdigt=0
           sfx(4)

           -- tiles that need support
           ty=-1
           if fget(tile,4) then
            ty=0

        -- everything else
           else
               additem(drops[tile] or tile,px+digx,py+digy,1,tile==9 and 20 or "block")
            mset(actx,acty,0)
        end

           -- break supported tiles
        while fget(bget(actx,acty+ty),4)
        do
         b=bget(actx,acty+ty)
         additem(drops[b] or b,px+digx,py+digy+ty*8,1,bget(actx,acty+ty)==9 and 20 or "block")
         mset(actx,acty+ty,0)
         ty-=1
        end

          end

   end

  -- place block
  elseif hitype=="block" then

   canplace=false

   -- platform offset
   if helditem.id==14 and pxv~=0 then
    acty+=1
   end

   -- check nearby tiles
   for ix=-1,1 do
    for iy=-1,1 do
     bl=bget(actx+ix,acty+iy)
     if abs(ix)+abs(iy)==1 and bget(actx,acty)==0 and (fget(bl,0) or fget(bl,2) or bl==14) then
      canplace=true
     end
    end
   end

   -- needs support?
   lt=bget(actx,acty+1)
   if fget(helditem.id,4) and not (fget(lt,0) or lt==14) then
    canplace=false
   end

   -- place block
   if canplace then
    if pdigt==0 and tile==0 then
           sfx(4)
           mset(actx,acty,helditem.id)
           helditem.amount-=1
          end
          pdigt+=1
          pdigt%=7
         else
          pdigt=0
         end

     -- hammer
     elseif tile~=0 and hitype=="hammer" and fget(tile,2) then

      -- break walls
      if pdigt==0 then
       sfx(4)
       local dropitem=drops[tile] or tile

       -- smash orbs
       if(tile==77)dropitem=48+orbsmash orbsmash+=1
       if(dropitem==50)additem(51,px+digx,py+digy,100)

    -- drop item
       additem(dropitem,px+digx,py+digy)
       mset(actx,acty,0)
      end
      pdigt+=1
      pdigt%=hiuse

     -- ranged weapon
     elseif hitype=="ranged" and pswing==hiuse then

   -- find ammo
   ammodamage,ammoid,ammosprite=false,-1,50

   for a in all(inventory) do
    if a.type==helditem.var and not ammodamage then
     if(not a.autoswing)a.amount-=1
     ammovel,ammodamage,ammoid,ammosprite=a.var/2,a.damage,a.id,a.usetime
    end
   end

   -- bullet
   if ammodamage then
             add(projectiles,{id=ammoid,x=px,y=py,xv=cos(pangle)*ammovel,yv=sin(pangle)*ammovel,damage=helditem.damage+ammodamage,bulletsprite=ammosprite})
    sfx(6)

   else
    pswing=0
    sfx(9)
   end

  -- life crystals and heals
  elseif hitype=="hpup" and btnp(5) and pswing==hiuse then
   if (php<php_max and not helditem.var) or (helditem.var and php_max<400) then
    if(helditem.var)php_max+=helditem.damage
    php+=helditem.damage
    helditem.amount-=1
    sfx(5)
   end

     -- boss summons
     elseif hitype=="summon" and (helditem.var~="marco" or timelight>1) and btnp(5) and (not helditem.autoswing or currentmusic==helditem.autoswing) then

   sfx(10)
   addmob(helditem.var,px-72,py-40,true)
   helditem.amount-=1

   reload()
   -- load plantera's sprite
   if(helditem.var=="plantera")tospr("eeee92f212ffff212f29eeeeeaae8222122ff2212228eaae54991221222222221221994544e812212222222212218e44eef811212222222212118feeee1f1111122222211111f1eeeee2f12f11222211f21f2eeeeee11a81f111111f18911eeeeee9aa9a1211112199aaaeeeeeaa98aa99a99aa9aa89aaeeeee98aa99a9cc9a89aa89eeeeeee999889e8ce9ce9aaeeeeeeeee898ceec8ec8e98eeeeeeeeeeeee88e8eee8ceeeeeeeeeeeeee8cee8ceeceeeeeeeeeeeeeeee8eee8eeeeeeeeeee",24,72,112)

  -- magic mirror
  elseif hitype=="aight imma head out" and btnp(5) then
   spawnplayer(php)
   sfx(11)
  end

 else
  pdigt=0
 end
 pswing,canturn=max(pswing-1),not (pswing>1 and (helditem.type=="melee" or helditem.type=="ranged"))

 -- change music
 local floortile=bget(px/8,py/8+1)
 if fget(floortile,0) then
  if floortile>=1 and floortile<=3 then
   worldmusic=py/8>47 and 12 or ((worldtime<17500 and worldtime>4300) and 0 or 53)
  -- jungle
  elseif floortile==6 or floortile==7 then
   worldmusic=31
  -- corruption
  elseif floortile>=10 and floortile<=12 then
   worldmusic=22
  end

 end

 local a=(hiuse-pswing)/(hiuse*2)+0.1
 wxo,wyo=
 cos(a)*(pflip and 8 or -8),
 sin(a)*8

end--player alive

-- update items
for i in all(items) do
 i.yv,i.timer=min(i.yv+0.04,1.2),max(i.timer-1)
 i.xv*=0.95

 local x,y=i.x,i.y

 -- go to player
 if not pdead and i.timer<=0 and abs(px-i.x)<24 and abs(py-i.y)<24 then
  i.xv+=(px-x)/50
  i.yv+=(py-y)/50
  if abs(px-x)<4 and abs(py-y)<4 then

   -- search inventory for this item
   local ininv,slot=false,1
   for a in all(inventory) do
    if a.id==i.id then
     ininv=true
    end
    if(not ininv)slot+=1
   end
   if ininv then
    inventory[slot].amount+=i.amount
    del(items,i)
   else

    -- life hearts
    if i.id==70 then
     php+=20
    -- normal
    else
     add(inventory,{id=i.id,amount=i.amount})
    end

    del(items,i)
   end
  end

 -- fall
 else

  -- collisions
  local gnd=bget(x/8,y/8+0.25)
  if fget(gnd,0) or gnd==14 then
   i.yv=min(i.yv)
  end
  if fget(bget(x/8,y/8-0.25),0) then
   i.yv=max(i.yv)
  end
  if fget(bget(x/8+0.25,y/8),0) then
   i.xv=min(i.xv)
  end
  if fget(bget(x/8-0.25,y/8),0) then
   i.xv=max(i.xv)
  end
 end

 i.x+=i.xv
 i.y+=i.yv

 -- delete oldest items
 if(#items>70)del(items,i)

end

-- update projectiles
for i in all(projectiles) do
 if i.bulletsprite~=109 then
  i.yv+=0.01
 end
 i.x+=i.xv
 i.y+=i.yv

 -- delete on collision
 if fget(bget(i.x/8,i.y/8),0) then
  del(projectiles,i)
 end

end

-- mob spawning
if t%(6+#mobs)==1 and not pdead then
 local ata,adi=rnd(),rnd(24)
 local atx,aty=(px+cos(ata)*(80+adi))\8,(py+sin(ata)*(80+adi))\8
 if not fget(bget(atx,aty),0) then
  local sptile=bget(atx,aty+1)

  -- spawn tile lists
  local forest=(worldtime<4300 or worldtime>17500) and
  "zombie,zombie,eye" or
  "gslime,gslime,gslime,bslime"

  -- lists
  local spawns={}
  spawns[10],spawns[11],spawns[12],spawns[6],spawns[7]=
  "eater","eater","eater","jslime,jslime,jbat,jbat,hornet,hornet,hornet,lacbeetle","jslime,jslime,jbat,jbat,hornet,hornet,hornet,lacbeetle"
  if py\8>76 then
   spawns[1],spawns[2],
   spawns[3],spawns[5],
   spawns[19],spawns[20],
   spawns[21],spawns[22]=
   "bat,bat,bat,skeleton,skeleton,rslime,rslime,rslime,cohbeetle",
   "bat,bat,bat,skeleton,skeleton,rslime,rslime,rslime,cohbeetle",
   "bat,bat,bat,skeleton,skeleton,rslime,rslime,rslime,cohbeetle",
   "bat,bat,bat,skeleton,skeleton,rslime,rslime,rslime,cohbeetle",
   "bat,bat,bat,skeleton,rslime,rslime,cohbeetle",
   "bat,bat,bat,skeleton,rslime,rslime,cohbeetle",
   "bat,bat,bat,skeleton,rslime,rslime,cohbeetle",
   "bat,bat,bat,skeleton,rslime,rslime,cohbeetle"
  elseif py\8>47 then
   spawns[1],spawns[2],spawns[3],spawns[5],spawns[19],spawns[20],spawns[21],spawns[22]=
   "bslime,bslime,yslime,yslime,rslime",
   "bslime,bslime,yslime,yslime,rslime",
   "bat,bslime,bslime,yslime,yslime,rslime",
   "bat,bslime,bslime,yslime,yslime,rslime",
   "bat,bslime,bslime,yslime,yslime,rslime",
   "bat,bslime,bslime,yslime,yslime,rslime",
   "bat,bslime,bslime,yslime,yslime,rslime"
  else
   spawns[1],spawns[2],spawns[3],spawns[5]=
   forest,forest,forest,forest
   spawns[6],spawns[7]=
   "jslime,jbat,jbat","jslime,jbat,jbat"
  end

  -- attempt to spawn
  if spawns[sptile] then
   addmob(rnd(split(spawns[sptile])),atx*8+4,aty*8+4)
  end
 end

end

-- update mobs
distance,pangle=120,pflip and 0.5 or 0
for i in all(mobs) do

 local playerang,
 id,ai,grav,collide,stoponwalls,enemy,bouncy,vmod,friction,boss=
 atan2(py-i.y,px-i.x)%1+0.5,
 i.id,i.ai,false,true,true,true,false,1,true,false
 i.spr=i.sprx

 if ai=="flying" then
  if i.rspr then
   i.sprx=1+t%20\10*2
  end
  i.flip=i.xv<0
 end

 if id=="eocservant" or id=="marcoservant" then
  collide,friction=false,false

 elseif id=="eoc" then
  i.spry,bossmusic=10+t%20\10*4,41

 elseif id=="kingslime" then

  i.scale,vmod,bossmusic,boss=
  0.6+i.hp/2000,1.5-i.hp/2500,41,true

  if i.hp~=i.php then
   addmob("bslime",i.x,i.y)
  end

  i.php=i.hp

 elseif id=="marco" then
  i.spry,bossmusic=19+t%20\10*6,41

 elseif id=="plantera" then
  i.spry,bossmusic,grav,collide,friction,i.ang,boss=
  11+t%20\10*4,41,false,false,false,playerang,true

  if i.t%(i.secondphase and 120 or 80)==0 then
   addmob("seed",i.x,i.y,true)
  end

  if i.hp<1500 then

   if not i.secondphase then
       for a=0,5 do
        addmob("tentacle",i.x,i.y,true,i)
       end
    i.secondphase=true
   end

   s,i.sprx=0.022+sin(t/70)/50,19

  else

   s=0.004+sin(t/100)/100
  end
  
  i.xv+=i.x<px and s or -s
  i.yv+=i.y<py and s or -s
  i.xv,i.yv,i.t=mid(-0.5,i.xv,0.5),mid(-0.5,i.yv,0.5),max(i.t+1)

 elseif id=="tentacle" then
  i.angv,grav,collide,friction,tang=
  i.angv or 0,false,false,false,i.host.ang+sin(t/100+i.rnd)*i.rnd

  i.sprx=1+(t+i.rnd*20)%20\10*2

  if i.ang>tang then
   i.angv-=0.0007
   if(i.ang>0.75 and tang<0.25)i.angv+=0.0005
  else--if i.ang<tang then
   i.angv+=0.0007
   if(tang>0.75 and i.ang<0.25)i.angv-=0.0005
  end
  i.angv,d=mid(-0.006,i.angv,0.006),
  30+sin(t/60+i.rnd)*14

  i.ang+=i.angv

  i.x,i.y=i.host.x+cos(i.ang-0.25)*d,i.host.y-sin(i.ang-0.25)*d

  if i.host.hp<=0 then
   i.hp=0
  end

 end

 -- keep angles 0-1
 i.ang%=1

 -- base ai
 if ai=="fighter" then
  i.xv+=sgn(px-i.x)*(i.air and 0.005 or 0.08)*vmod
  grav,i.xv=true,mid(-0.5*vmod,i.xv,0.5*vmod)
  i.flip=i.xv<0
  i.spr+=i.x/8%2

 elseif ai=="slime" then
  grav,i.jumpt,i.d,stoponwalls,
  v=
  true,i.jumpt or 60,i.d or sgn(rnd()-.5),false,
  i.angry and sgn(px-i.x) or i.d

  i.jumpt-=0.5*vmod

  if i.jumpt<=0 and not i.air then
   i.jumpt,i.yv,i.xv=
   80+rnd(40),-1.2-rnd(),v*vmod
  end

  i.spr+=(i.jumpt<30 and t%20\10 or t%40\20)*i.w

 elseif ai=="flying" then
  bouncy,v,angv,i.ang=
  true,0.02,0,playerang

  i.spr+=t%20\10

  i.xv,i.yv=mid(-vmod,i.xv+cos(i.ang-0.25)*0.01*vmod,vmod),mid(-0.5*vmod,i.yv-sin(i.ang-0.25)*0.01*vmod,0.5*vmod)

 elseif ai=="eye" then
  bouncy,i.xv,i.yv=
  true,mid(-0.8,i.xv+(px-i.x)*0.001,0.8),mid(-0.5,i.yv+(py-i.y)*0.001,0.5)

  i.spr+=t%20\10
  i.ang=atan2(i.yv,i.xv)%1+0.5

 elseif ai=="projectile" then
  collide,friction,i.ang=false,false,atan2(i.yv,i.xv)%1+0.5

  if i.angry then
   i.xv,i.yv,i.angry=cos(playerang-0.25),-sin(playerang-0.25),false
  end

  i.x+=i.xv
  i.y+=i.yv

 elseif ai=="eoc" then
  collide,friction,i.ang,boss=false,false,playerang,true

  if i.hp<1200 then
   i.damage,i.defence=23,0
   if(not i.secondphase)i.dashstate,i.secondphase=1,true
  end

  -- leave
  if timelight<=1 then
   i.yv-=0.02

  -- spiiin
  elseif i.dashstate==1 then
   i.av,i.a=i.av or 0,i.a or i.ang
   i.av+=0.002
   i.a+=i.av

   i.ang=i.a

   if(i.av>0.2) sfx(10) i.dashstate=false i.sprx=i.id=="marco" and 12 or 10

  -- dash
  elseif i.dashstate then

   i.v,i.va=i.v or 0,i.va or 0
   if i.t%120==0 then
    i.v,i.va=(i.secondphase and 2.4 or 1.6)*vmod,0.25-i.ang
    if(i.secondphase)sfx(10)
   end
   i.xv,i.yv=cos(i.va)*i.v,sin(i.va)*i.v
   i.v*=0.98

  -- float above
  else

   if i.t%120==0 and (i.id=="marco" or not i.secondphase) then
    addmob(id.."servant",i.x,i.y,#mobs<12)
   end

   if i.t%25==0 and i.id=="marco" and i.secondphase then
    addmob("laser",i.x,i.y,true)
    sfx(3)
   end

   if i.x<px then
    i.xv+=i.xv<0 and 0.1 or 0.05
   end
   if i.x>px then
    i.xv-=i.xv>0 and 0.1 or 0.05
   end

   if i.y<py-40 then
    i.yv+=i.yv<0 and 0.1 or 0.05
   end
   if i.y>py-40 then
    i.yv-=i.yv>0 and 0.1 or 0.05
   end

   i.xv,i.yv=mid(-vmod,i.xv,vmod),mid(-vmod,i.yv,vmod)

  end

  i.t=max(i.t+1)

  -- loop states
  if i.t%360==0 and i.dashstate~=1 then
   i.dashstate=not i.dashstate
  end

 end

 -- fall
 if grav then
  i.yv+=0.08
 end

 -- pixel scale
 pxw,pxh=i.w*4*i.scale,i.h*4*i.scale

 -- collide with player
 if enemy and not pdead and piframes<=0 and abs(px-i.x)<pxw and abs(py-i.y)<pxh then
  php-=i.damage
  piframes,pxv,pyv=30,sgn(px-i.x)*1.2,-1.2
  sfx(0,-1,phurt,10)
 end

 -- collide with blocks
 if collide then
  left,right,up,down,xr,yr=false,false,false,false,pxw,pxh

     -- down
     d,e,i.air=0,false,true
     repeat
      local l,r=bget((i.x-pxw+1)/8,(i.y+pxh+d)/8),bget((i.x+pxw-2)/8,(i.y+pxh+d)/8)
      if i.yv>0 and (fget(l,0) or l==14 or fget(r,0) or r==14) then
       i.y+=d
       i.yv,e,down,i.air=bouncy and -i.yv or 0,true,true,false
      end
      d+=1
     until d>i.yv or e

     -- up
     d,e=0,false
     repeat
      if i.yv<0 and (fget(bget((i.x-pxw+1)/8,(i.y-pxh-1+d)/8),0) or fget(bget((i.x+pxw-2)/8,(i.y-pxh-1+d)/8),0)) then
    i.yv,e=bouncy and -i.yv or 0,true
    i.y+=d
      end
      d-=1
     until d<i.yv or e

     -- right
     d,e=0,false
     repeat
      if i.xv>0 and (fget(bget((i.x+pxw-1+d)/8,(i.y-pxh)/8),0) or fget(bget((i.x+pxw-1+d)/8,(i.y+pxh-1)/8),0)) then
       i.xv,e,right=bouncy and -i.xv or 0,true,true
       i.x+=d
      end
      d+=1
     until d>i.xv or e

     -- left
     d,e=0,false
     repeat
      if i.xv<0 and (fget(bget((i.x-pxw+d)/8,(i.y-pxh)/8),0) or fget(bget((i.x-pxw+d)/8,(i.y+pxh-1)/8),0)) then
       i.xv,e,left=bouncy and -i.xv or 0,true,true
       i.x+=d
      end
      d-=1
     until d<i.xv or e

 end

 -- ground/air friction
 if(friction)i.xv*=down and 0.8 or 0.99

 -- move
 i.x+=i.xv
 i.y+=i.yv

 -- after collisions ai

 -- zombie jump at walls and near but below player
 if ai=="fighter" and down and (left or right or (i.y>py+2 and abs(px-i.x)<16)) then
  i.yv=-2.2

 -- slime step over tiles
 elseif ai=="slime" and (left or right) and not down then
  i.xv+=(v-i.xv)/10

 end

 -- prevent plr hit projectiles
    local hit=false
 if ai~="projectile" then

     --collide with plr projectiles
     for a in all(projectiles) do
      if i.iframes==0 and abs(a.x-i.x)<pxw and abs(a.y-i.y)<pxh then
       i.hp-=max(a.damage-i.defence/2,1)
       if(i.knockback>0)i.xv,i.yv=sgn(i.x-a.x)*i.knockback,-i.knockback
       hit=true
       del(projectiles,a)
      end
     end

     -- player hit enemies
     if helditem.melee and pswing>0 and i.iframes==0 and abs(px+wxo-i.x)<pxw+3 and abs(py+wyo-i.y)<pxh+3 then
   i.hp-=max(helditem.damage-i.defence/2,1)
   if(i.knockback>0)i.xv,i.yv=(pflip and -1 or 1)*i.knockback,-i.knockback
   hit=true
     end

 end
    i.iframes=max(i.iframes-1)

 -- hit effects
 if hit then
  i.iframes,i.angry=pswing,true
  sfx(6)
 end

 -- get line of sight
 cansee,sight,d=true,1,dist(px-i.x,py-i.y)

 -- check if closest or boss
 if d<distance or boss then

  ba=0.25-atan2(py-i.y,px-i.x)
  repeat
   if(ai=="projectile" or fget(bget((i.x-cos(ba)*sight)/8,(i.y-sin(ba)*sight)/8),0))cansee=false
   sight+=1
  until sight>d or not cansee

  if cansee then
   distance,pangle=d,ba
   if(boss)distance=0
  end
 end

 -- die
 if i.hp<=0 then
  if i.drop then
   it=split(i.drop,":")
   for a=1,it[2]-(boss and 0 or rnd(2)\1) do
    additem(it[1],i.x,i.y)
   end

   -- randomly drop health pickups
   if(rnd(12)<1)additem(70,i.x,i.y)
  end
  del(mobs,i)

 -- despawn
 elseif d>360 or pdead then
  del(mobs,i)
 end

end

piframes=max(piframes-1)

-- player die
if php<=0 and not pdead then
php,pdiet,pdead,pswing=0,60,true,0
sfx(27)
end

-- camera
cam_x,cam_y=mid(0,px-64,256*8-128),mid(0,py-64,128*8-128)

end--invopen

if helditem.id==17 then
add(lights,{px,py})
end

-- toggle inventory
if btnp(6) and not pdead then
poke(0x5f30,1)--suppress pause
sfx(invopen and 8 or 7)
invopen=not invopen
updaterecipes()
end

php=min(php,php_max)

timelight=1

-- night
if worldtime<2000 or worldtime>18000 then
timelight=5

-- edawn/ldusk
elseif worldtime<3000 or worldtime>17000 then
timelight=4

-- dawn/dusk
elseif worldtime<4000 or worldtime>16000 then
timelight=3

-- ldawn/edusk
elseif worldtime<5000 or worldtime>15000 then
timelight=2

end

-- music
nextmusic=bossmusic or worldmusic
if nextmusic~=currentmusic or stat(24)==-1 then
music(-1,1000)
currentmusic=nextmusic
end
if stat(24)==-1 then
music(nextmusic,1000)
end

end

t=max(t+1)

end

function _draw()

smolmap()

if mode==pmode then

pal()
palt(0,false)
palt(14,true)

cls(14)

pal(split"130,132,4,9,10,5,134,131,3,139,7,1,140,12,136",1)

if mode<3 then
 -- bg
 map(28,0,-(t/10%512),0,80,16)
 -- title logo
 rspr(64,20,sin(t/500)/40,18,1,4)
 cprint("a terraria demake",40)
 cprint(mode==1 and "drop a player file here" or "drop a world file here",76)
 cprint("(or press 🅾️ to start over)",84,7,1,63)

 cprint("press ❎ for the player editor",120,7,1,63)

-- game
else

-- bg
map(28,0,-(cam_x/4%512),28-cam_y/4,80,20)

-- underground bg
e=flr(cam_y)
rectfill(0,296-e,127,640,1)
rectfill(0,608-e,127,1040,12)

camera(cam_x,e)

-- bigmap mk.2
bigmap()

-- treetops, walls and light tiles
for iy=cam_y\8-1,cam_y\8+18 do
 for ix=cam_x\8-1,cam_x\8+17 do

  -- treetops
  tile=bget(ix,iy)
  if tile==82 and bget(ix,iy-1)~=82 then
   -- check ground tile
   y=iy
   repeat
    y+=1
   until bget(ix,y)~=82
   s,w,o,b=142,2,4,bget(ix,y+1)
   if(b==6)s=137w=3o=8
   if(b==10)s=140

   -- draw treetop
   spr(s,ix*8-o,iy*8-16,w,2)

  -- light sources
  elseif fget(tile,7) then
   add(lights,{ix*8+4,iy*8+4})
  end

  -- walls
  if fget(tile,6) then
   -- check for walls
   wall=false
   for ox=-1,1 do
    for oy=-1,1 do

     wtile=bget(ix+ox,iy+oy)
     if fget(wtile,2) then
      wall=wtile
     end

    end
   end

   if wall then
    spr(wall,ix*8,iy*8)
   end

  end

 end
end

map()

smolmap()

for i in all(items) do
 sspr(i.id%16*8,i.id\16*8,8,8,i.x-2,i.y-2,4,4)
end

--pal(11,14)

-- draw mobs
for i in all(mobs) do

 local sc=i.scale*4

 -- tentacle lines
 if(i.host)line(i.x,i.y,i.host.x,i.host.y,1)

 -- plantera's phase 2 palette
 if i.id=="plantera" and i.sprx==19 then
  -- eoc's colours
  pal(7,10)
  pal(6,9)

  -- plantera's colours
  pal(1,8)
  pal(2,9)
  pal(15,10)
 end

 -- do palette swapping
 if i.pal then
  pal(split(i.pal,":"))
 end

 -- rotatey
 if i.rspr then
  rspr(i.x,i.y,i.ang,i.sprx,i.spry,i.w/2)

 -- non-rotatey
 else

  sspr(flr(i.spr)%16*8,i.spr\16*8,i.w*8,i.h*8,i.x-i.w*sc,i.y-i.h*sc,i.w*2*sc,i.h*2*sc,i.flip)
 end

    -- restore palette
    pal()
    palt(0,false)
    palt(14,true)

 -- extra details
 if i.id=="kingslime" then
  -- crown
  spr(206,i.x-8,i.y-i.h*sc-5.5+(i.spr==233 and sc/2 or 0),2,1)
 end
end

-- projectiles
for i in all(projectiles) do
 mset(1,1,i.bulletsprite)
 rspr(i.x,i.y,atan2(i.yv,i.xv)+0.5,1,1,0.5)
end

-- player
if not pdead and (piframes==0 or t%4<2) then

-- i made it worse
spr(pswing~=0 and
132+((hiuse-pswing)/hiuse*3)%3
or (pair and 130 or 128)+(pxv==0
and 0 or px/8%2),px-4,py-4,1,1,
pflip)
-- aaaaaaaaaaaaaaaaaaaaaaaaaaaaa

end

-- player tool
mset(1,1,swingitem.id)
if pswing>0 then
    --vflip=false
    if swingitem.type=="ranged" then
     pangle%=1
     wxo,wyo=cos(pangle)*8,sin(pangle)*8
     rspr(px+wxo,py+wyo,-pangle,1,1,0.5,false,pangle<=0.75 and pangle>0.25)
    else
  rspr(px+wxo,py+wyo,atan2(wyo,wxo)-(pflip and 0.625 or 0.375),1,1,0.5,not pflip)
    end

 -- break progress
 if digging then
  spr(192+pdigt/hiuse*2,(px+digx)\8*8,(py+digy)\8*8)
 end
end

camera()

-- inventory
for id,i in pairs(inventory) do
 invy=id*13-invsel*13+64
 if invside==1 and id==invsel and not hoveritem then
  sspr(89,84,12,12,115,invy-14)
 else
  sspr(101,86,10,10,116,invy-13)
 end
 spr(i.id,117,invy-12)
 if(i.amount and i.amount>1)print(i.amount,116,invy-8,11)

end

-- hovering item
if hoveritem then
 sspr(89,84,12,12,103,50)
 spr(hoveritem.id,105,52)
 --print(hoveritem.type,64,64)
end

-- craft
selectedcraft=0
if invopen then

 -- trashed item slot
 pal(12,1)
 pal(13,15)
 sspr(101,86,10,10,116,116)
 if trashslot then
  spr(trashslot.id,117,117)
 end
 pal(12,12)
 pal(13,13)

 for id,i in pairs(craftable) do
  local out=craftable[id][1]

  invy=id*13-craftsel*13+64
  if invside==0 and id==craftsel and avail then
   sspr(89,84,12,12,1,invy-14)
   selectedcraft=out[1]
  else
   sspr(101,86,10,10,2,invy-13)
  end
  spr(out[1],3,invy-12)
  if(out[2] and out[2]>1)print(out[2],2,invy-8,11)

 end

end

-- health bar
for i=0,(php_max-1)\20 do
 a,b=i*8%80,i\10*8
 sspr(111,87,9,9,a+1,b+1)
 if(i<=(php-1)\20)sspr(111,80,7,7,a+2,b+2)
end
bprint(flr(php).."/"..php_max,2,12+(php_max-20)\200*8)

-- menu label / item name
if invopen and #inventory>0 then
 cprint(split"crafting,inventory"[invside+1],121)

 cprint(avail and tilenames[
  (hoveritem and hoveritem.id) or
  (invside==1 and inventory[invsel]
   and inventory[invsel].id) or
  selectedcraft] or "need more materials",48,11,6,67)

end

if pdead then
 cprint("you were slain",60,11,15)
 if(pdiet==0)cprint("jump to respawn",68,11,15)
end

-- get light level
local d=120
for i in all(lights) do
 d=min(dist(i[1]-px,i[2]-py),d)
end
distlight,
surfacelight=
flr(mid(1,d/8-6,5)),
flr(mid(1,py/24-12,5))

-- apply light level palette
pal(split(palettes[min(distlight,max(surfacelight,timelight))]),1)

-- sky colour
pal(14,split"12,140,1,129,0"[timelight],1)

end

end

pmode=mode

end

---function music()return 0 end

-->8
-- worldgen

function genprint(t,t2)
 poke(0x5f30,1)--suppress pause
 pal(split"130,132,4,9,10,5,134,131,3,139,7,1,140,12,136",1)
 cls()
 cprint(t,62)
 cprint(t2 or "",110)
 flip()
end

function generate()

 -- go to bigmap
 bigmap()

 function maketunnel()
  -- cave tunnel
  for ix=-s,s do
   for iy=-s,s do
       reptil=bget(cx+ix,cy+iy)
       if(reptil<64 and reptil>0)reptil+=64
       -- place walls above height
    mset(cx+ix,cy+iy,cy<60 and (reptil==75 and 66 or reptil) or 0)
   end
  end
    end

    worlddir,ry,ra,tunnel,dirttile=rnd()-.5,30,0,0,2
 surfacecaves,crystals,ores=0,0,0
 genprint"sculpting the land"
    for ix=0,255 do

     -- ground height
     ra=mid(-0.22,ra+rnd()/5-0.1,0.22)
     ry+=sin(ra)/4

     -- limit ground variation
     if ry<20 or ry>37 then
      ra,ry=0,mid(20,ry,37)
     end

     -- tunnels
     if rnd(50)<1 and tunnel<=0 then
      tunnel=flr(rnd(8)+6)
     end

     -- add tiles
     for iy=0,127 do

   -- corrupt side, jungle side
   
   if worlddir<0 then
    cs,js=ix>172,ix<84
   else
    cs,js=ix<84,ix>172
   end

      -- stone
      if iy>ry+10 then
       mset(ix,iy,js and 7 or 3)

   -- dirt
      elseif iy>=flr(ry) then

       -- determine dirt tile
       dirttile=js and 7 or (cs and 11 or 2)

       mset(ix,iy,dirttile)

      end

     end

  -- generate tunnels
     if tunnel>0 then
      for i=1,5 do
    mset(ix,ry-i,i>3 and dirttile or (cs and 66 or dirttile+64))
      end
      tunnel-=1
     end
    end

 genprint"making spaghetti caves"
 for i=0,50 do
  a,cx,cy,s=rnd(),rnd(256),rnd(178-i/2)+50+i/2,rnd(2)+1
  for l=0,10+rnd(80) do
   av=(rnd(2)-1)/12
   a+=av

   cx,cy=cx+cos(a),cy+sin(a)
      -- cave
      maketunnel()
  
  end
  
 end

 genprint"adding surface caves"
    repeat
     ix,iy=rnd(216)+20,0

     -- go to ground
     repeat
      iy+=1
      dirttile=bget(ix,iy)
      valid=dirtitle==2 or dirttile==7 or dirttile==11
     until valid or iy>32

  -- don't spawn them underground
  if valid then

         -- circle
      r=7+rnd(4)
         for y2=-r,r do
          x2=flr(sqrt(abs(y2*y2-r*r)))
          for ox=ix-x2,ix+x2 do
           mset(ox,iy+y2+3,dirttile)
          end
         end
         
         -- cave
         xo,s=sgn(rnd()-.5),1
         ca,cx,cy=0.5-0.25*xo,ix+xo*r,iy
         for l=0,80+rnd(40) do
          ca=mid(0.25,ca+(rnd(2)-1)/8,0.75)

          cx+=cos(ca+0.25)
          cy+=sin(ca+0.25)
   
          -- cave tunnel
          maketunnel()

         end
         
   surfacecaves+=1
        end

    until surfacecaves>=4

 genprint"digging corrupt chasms"
 repeat

  -- go to ground
  x,y=rnd(256),0
  repeat
   y+=1
  until bget(x,y)==11 or y>40

  -- has found corrupt?
  if y<=37 then

   for l=2,30+rnd(20) do

          -- make tunnel
          s=3
    x+=rnd(2)-1
          for ix=-s,s do
           for iy=-s,s do
            mset(x+ix,y+iy+l,((abs(ix)>=s-1 or iy>=s-1) and bget(x+ix,y+iy+l)~=76) and 12 or 76)
            mset(x,y+l,77)
           end
          end
          
   end
  end

     -- count chasms
     chasms=0
     for i=0,32767 do
   if bget(i%256,i\256)==77 then
    chasms+=1
   end
        end

    until chasms>=3

 genprint"adding various details"
    -- post processing
    for ix=0,255 do
     for iy=0,127 do

      -- add grass on dirt and mud
      if (bget(ix,iy)==2 or bget(ix,iy)==7 or bget(ix,iy)==11) and not fget(bget(ix,iy-1),0) then
       mset(ix,iy,bget(ix,iy)-1)

    -- trees
    if rnd(2)<1 then
     cantree,th=true,4+rnd(5)

     for y=1,th+4 do
      if bget(ix,iy-y)~=0 then
       cantree=false
      end
     end

     if cantree then
         for y=2,th do
       mset(ix,iy-y,82)
         end
      mset(ix,iy-1,83)
     end

    end
    -- end trees

      end

     end
    end

 genprint"hiding life crystals"

 repeat
  ix,iy=rnd(256),rnd(58)+60
  -- life crystals
  if bget(ix,iy)==0 and fget(bget(ix,iy+1),0) then
   mset(ix,iy,9)
   crystals+=1
  end
 until crystals>=20

 genprint"placing metals and clay"
 repeat
  ix,iy=rnd(256),rnd(38)+ores/2

  -- vein
  set=false
  for i=0,3+rnd(4)+iy/30 do
   ix+=rnd(2)-1
   iy+=rnd(2)-1
   if fget(bget(ix,iy),0) and bget(ix,iy-1)<82 then
    mset(ix,iy,19+ores/46)
    set=true
   end
  end
  if(set)ores+=1
 until ores>=180

end

-->8
-- functions

function addmob(id,x,y,override,host)

 local stats=split[[gslime|14|6|0|59:1|slime|0.8|1|0|0| 176|0|1|1
  ,bslime|25|8|2|59:2|slime|1|1|0|0| 160|0|1|1
  ,rslime|42|15|4|59:2|slime|1|1|1|0| 144|0|1|1|0:1:6
  ,yslime|42|16|7|59:3|slime|1|1|1|0| 144|0|1|1|3:4:5
  ,jslime|50|18|6|59:4|slime|1|1|1|0| 144|0|1|1|9:10:5
  ,eye|55|18|2|54:1|eye|1|1|0|1| 1|10|1|1
  ,zombie|40|14|6|59:3|fighter|0.5|0.5|0|0| 146|0|1|1
  ,eater|38|22|8|61:1|flying|0.5|1|0|1| 1|12|1|1
  ,bat|15|13|2|86:1|flying|0.8|1|0|0| 162|0|1|1|12:13:5
  ,jbat|30|20|4|89:1|flying|0.8|1|0|0| 162|0|1|1|1:2:4
  ,hornet|46|26|12|60:1|flying|0.5|1.5|0|0| 166|0|1|1
  ,eocservant|8|12|0|70:1|flying|1|0.6|0|1| 1|14|1|1
  ,marcoservant|20|12|0|70:1|flying|1|0.9|0|1| 1|16|1|1
  ,skeleton|45|20|8|52:8|fighter|0.5|1.1|0|0| 182|0|1|1
  ,eoc|2400|10|12|23:18|eoc|0|1|0|1| 5|10|3|3
  ,kingslime|1800|22|10|22:12|slime|0|1|1|0| 230|0|3|2
  ,marco|3000|22|13|27:1|eoc|0|1|0|1| 6|19|4|4
  ,plantera|3500|26|16|90:1||0|1|0|1| 23|11|3|3
  ,seed|1|19|0|0:0|projectile|0|1|1|1| 1|18|1|1
  ,laser|1|13|0|0:0|projectile|0|1|1|1| 1|20|1|1
  ,tentacle|200|16|10|70:1||0|1|0|1| 1|22|1|1| 8:9:3:4:5:9:10:8:9:10:11:12:13:14:10
  ,cohbeetle|40|20|10|93:1|fighter|1|0.8|0|0|164|0|1|1|1:15:15:3:2
  ,lacbeetle|40|20|10|93:1|fighter|1|0.8|0|0|164|0|1|1|2:15:3:1:0
  ]]

 for k,i in pairs(stats) do
     if split(i,"|")[1]==id then
         s=split(stats[k],"|")
         break
        end
 end

 -- mob cap
 if (#mobs<8 or override) and s then

  add(mobs,{id=id,x=x,y=y,xv=0,
  yv=0,spr=tonum(s[11]),scale=1,hp=s[2],
  damage=s[3],defence=s[4],
  w=s[13],h=s[14],ang=0,iframes=0,
  drop=s[5],t=0,ai=s[6],
  knockback=s[7],vmod=s[8]==1,
  angry=s[9]==1,rspr=s[10]==1,sprx=s[11],spry=s[12],
  pal=s[15],host=host,rnd=rnd()})
 end

end

function additem(id,x,y,amount)
 add(items,{id=id,amount=amount or 1,x=x,y=y,xv=(rnd(2)-1)/3,yv=-1,timer=60})
end

function bprint(t,x,y,c1,c2)
 for ix=-1,1 do
  for iy=-1,1 do
   ?t,x+ix,y+iy,c2 or 6
  end
 end
 ?t,x,y,c1 or 11
end

function cprint(t,y,c1,c2,x)
 bprint(t,(x or 65)-#tostr(t)*2,y,c1,c2)
end

-- check crafting recipes
function updaterecipes()
 avail,craftable=true,{}
 for i in all(recipes) do

  -- station
  s,cancraft=i[1][3],false

  -- check station
  for ix=-3,3 do
   for iy=-3,3 do
    if not s or bget(px/8+ix,py/8+iy)==s then
     cancraft=true
    end
   end
  end

  -- check ingredients
  for r in all(i[2]) do
   a=r[2] or 1--amount
   for slot in all(inventory) do
    if slot.id==r[1] then
     a-=slot.amount
    end
   end

   -- can't craft if not enough
   if(a>0)cancraft=false

  end

  -- add recipe
  if(cancraft)add(craftable,i)
 end

end

function spawnplayer(hp)
 bigmap()
 pdead,php,px,py,pxv,pyv,piframes=false,hp or php_max,256*4,0,0,0,60
 repeat
  py+=8
 until fget(bget(px/8,py/8),0)
 py-=4
end

function bigmap()
 poke(0x5f56,0x80,0)
end

function smolmap()
 poke(0x5f56,0x20,128)
end

function tospr(str,w,x,y)
for i=0,#(str or "")-1 do
sset(x+i%w,y+i\w,tonum("0x"..sub(str,i+1,_)))
end
end

function loadplayer(pstr)

local plr=split(pstr,"|")
local hp=split(plr[1])
pgfx,php,php_max,inventory,phurt=tostr(plr[3] or ""),hp[1],hp[2],{},hp[3] or 0


for i in all(split(plr[2])) do
 if #i<10 then
  local it=split(i,":")
  add(inventory,{id=it[1],amount=it[2]})
 end
end
end

function bget(x,y)
 return x>=0 and y>=0 and x<256 and y<128 and mget(x,y) or 255
end

-- auto restore player sprites
_reload=reload
function reload()
 _reload()
 tospr(pgfx,56,0,64)
end
-->8
-- borrowed functions

-- tHErOBOz (mODIFIED BY CUBEE)
--[[ 🐱
reduced size to clip corners
fixed horizontal flipping
added vertical flipping
saved some tokens
]]
function rspr(x,y,sw_rot,mx,my,r,flip,vflip)
if(flip or vflip)sw_rot=-sw_rot
local cs,ss,ssx,ssy,cx,cy,yf=cos(sw_rot),-sin(sw_rot),mx-0.3,my-0.3,mx+r,my+r,vflip and -1 or 1
ssy-=cy-0.1
ssx-=cx-0.1
local sx,sy,delta_px=cs*ssx+cx,-ss*ssx+cy,-ssx*8
if(flip)delta_px=-delta_px x-=1
for py=y-abs(delta_px)*yf,y+abs(delta_px)*yf,yf do
tline(x-delta_px,py,x+delta_px,py,sx+ss*ssy,sy+cs*ssy,cs/8,-ss/8)
ssy+=0.125
end
end

--freds72
function dist(dx,dy)
local maskx,masky=dx>>31,dy>>31
local a0,b0=(dx+maskx)^^maskx,(dy+masky)^^masky
if a0>b0 then
return a0*0.9609+b0*0.3984
end
return b0*0.9609+a0*0.3984
end
