-- title: conveyor belt simulator
-- author: modle
-- description:
--     this is prototype/reference code
--     beware inefficiencies/inconsistencies
-- controls:
--     dpad/arrows: move the player sprite
--     LMB/press: manage conveyor segments
--         with direction selected: place conveyor/change conveyor direction
--         with no direction selected: select segment direction by clicking on an arrow

-----------
-- GLOBALS :(
-----------

actors = {} -- all actors
numbers = {}
player = nil
reference_frame = 0
max_frames = 60


-----------
-- CORE
-----------

function _init()
    -- create sprites
    create_numbers()

    -- make player
    player = add_actor(1,2,2,'player')
    player.frames = 4
    troggle = add_actor(5,7,7,'troggle')
end

function _update()
    control_player()
    foreach(actors, move_actor)
    manage_frame()
end

function _draw()
    -- clear screen
    cls()

    handle_camera()

    -- draw map
    map()

    foreach(numbers, draw_actor)
    foreach(actors, draw_actor)
end


-----------
-- NUMBERS
-----------

function create_numbers()
    start_id = 15
    end_id = 63
    total_rows = 7
    printh('calculating number positions')
    for j=start_id,end_id do
        x = flr((j - start_id) / total_rows) * 2 + 2
        y = ((j - start_id) % total_rows) * 2 + 2
        printh(start_id .. ',' .. end_id .. ',' .. x .. ',' .. y)
        a = make_actor(j, x, y, 'number')
        add(numbers, a)
    end
end


-----------
-- ACTORS
-----------

function add_actor(sprite_id, x, y, name)
    a = make_actor(sprite_id, x, y, name)
    add(actors,a)
    return a
end

-- make an actor
-- and add to global collection
-- x,y means center of the actor
-- in map tiles
function make_actor(sprite_id, x, y, name)
    a={
        sprite_id = sprite_id,
        x = x,
        y = y,
        name = name,
        dx = 0,
        dy = 0,
        frame = 0,
        t = 0,
        friction = 0.35,
        bounce = 0.3,
        direction_x = 1,
        direction_y = 1,
        frames = 2,
        stationary = false,

        -- half-width and half-height
        -- slightly less than 0.5 so
        -- that will fit through 1-wide
        -- holes.
        w = 0.4,
        h = 0.4
    }

    return a
end

function move_actor(a)
    -- calculate new movement speed
    to_move_x = a.dx
    to_move_y = a.dy

    -- add movement speed to position
    a.x += to_move_x
    a.y += to_move_y

    -- handle animation speed
    frame_move_default = 0.05
    frame_move_x = frame_move_default
    frame_move_y = frame_move_default
    -- if there's any input, vary animation with that
    if (abs(a.dx) != 0) frame_move_x = abs(to_move_x) * 2
    if (abs(a.dy) != 0) frame_move_y = abs(to_move_y) * 2
    -- set to default if too low
    if (frame_move_x < frame_move_default) frame_move_x = frame_move_default
    if (frame_move_y < frame_move_default) frame_move_y = frame_move_default

    -- adjust the frame
    -- only need to adjust based on fastest direction
    -- or diagonals will double animation speed
    if frame_move_x > frame_move_y then
        a.frame += frame_move_x
    else
        a.frame += frame_move_y
    end
    -- sets equal to remainder
    a.frame %= a.frames

    a.t += 1
    -- apply friction
    -- (comment for no inertia)
    a.dx *= (1-a.friction)
    a.dy *= (1-a.friction)
end

function draw_actor(a)
    local sx = (a.x * 8) - 4
    local sy = (a.y * 8) - 4
    spr(a.sprite_id + a.frame, sx, sy, 1, 1, a.direction_x==-1, a.direction_y==-1)
end

function add_things()
    if reference_frame == 0 then
        -- add a chomper
        chomper = make_actor(28, flr(rnd(10))+1, flr(rnd(5))+1, 'chomper')
        add(actors, chomper)
        -- add a flower?
        -- add(actors, make_actor(27, flr(rnd(10))+1, flr(rnd(5))+1, 'flower'))
    end
end


-----------
-- CONTROLS
-----------

function control_player()
    accel = 0.05
    if (btn(0)) player.dx -= accel player.direction_x=-1
    if (btn(1)) player.dx += accel player.direction_x=1
    if (btn(2)) player.dy -= accel
    if (btn(3)) player.dy += accel
end

function handle_camera()
    room_x=flr(player.x/16)
    room_y=flr(player.y/16)
    camera(room_x*128, room_y*128)
end


-----------
-- UTILITIES
-----------

function copy_sprite(sprite, new_id, new_name)
    new_sprite = make_actor(sprite.sprite_id, sprite.x, sprite.y, sprite.name)
    if (new_id)    new_sprite.sprite_id = new_id
    if (name)      new_sprite.name = new_name
    if (direction) new_sprite.direction = sprite.direction
    return new_sprite
end

function animate_stationary(a)
    -- why 0.25? quarter of a square
    a.frame += 0.25
    a.frame %= a.frames
end

function manage_frame()
    reference_frame += 1
    reference_frame %= max_frames
end
