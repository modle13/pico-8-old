-- title: conveyor belt simulator
-- author: modle
-- description:
--     this is prototype/reference code
--     beware inefficiencies/inconsistencies
-- controls:
--     dpad/arrows: move the player sprite
--     LMB/press: manage conveyor segments
--         with direction selected: place conveyor/change conveyor direction
--         with no direction selected: select segment direction by clicking on an arrow
--     button map:
--         1: left/right (kb arrows and gamepad dpad)
--         2: up         (kb arrows and gamepad dpad)
--         3: down       (kb arrows and gamepad dpad)
--         4: z/c/n (kb), y/a (gamepad)
--         5: x/v/m (kb), x/b (gamepad)
--     button 4 press:
--         perform cursor action if cursor mode
--             paint belt or change target direction if belt segment selected
--             delete belt or (wip) structure if delete selected
--             (wip) paint structure if structure selected
--         interact with object if in player sprite mode
--             (wip) interact with element in current square
--                 picking up objects
--                 activating objects/structures
--     button 4+5 press:
--         cancel selection if cursor mode
--         toggle between cursor mode and player sprite mode if no selection
--     button 5 press:
--         perform action on cursor, context-dependent
--             rotate cursor belt direction
-- features:
--     place, rotate, delete belts
--     objects on belt will move in that belt's direction
--     structures to handle resource production and storage
--         structures will produce materials if output not full
--         check for presence of material
--         building will process immediately and output result
--         types
--             coal     - consumes log,  produces coal
--             logs     - produces log
--             firewood - consumes log,  produces firewood
--             planks   - consumes log,  produces planks
--             gold     - produces gold
--             keys     - consumes gold, produces key
--     change building type when cursor is a different type of building
--     map access limited to a single screen
-- known bugs:
--     (need to check if still a problem) player sprite appears to ride 2 conveyor paths
--         this is due to the collision offset math
--         also sometimes gets stuck
--             if belt doesn't exist and is placed under player later
--     belt priority is first found, so left moving into up can cause sprite to move up
--         even if sprite is mostly on the left-moving belt
-- UX enhancement opportunities
--     use btn 5+direction to toggle cursor mode instead of 4+5
--         this is going to be difficult to get right
--         might have to reduce the amount of button operations
--     cursor toggle mode OFF should:
--         cancel cursor selection
--         hide cursor
--         move cursor back to start point
--     cursor toggle mode ON should:
--         show cursor
--         make player sprite sleep?
--         change background/border?
-- future changes:
--     player sprite will interact with structures
--     player sprite will interact with objects to move them around
--         picking up logs, etc, off of belts or the ground
--         dropping logs, etc, onto belts or the ground
--     structures that produce materials:
--         color-coded materials with legend, due to pixel limits
--         materials can only be moved with belts
--         pick from building selector to place
--         tree > log > firewood
--     objectives:
--         produce X logs
--         produce X firewood
--     victory screen
--     reset screen with next level, including starting belt positions
--     level select screen
--     stagger building consumption by type to have better visibility of resources
--         so a resource isn't produced and consumed without being seen
--     set upper limit on how many resources can be stored
-- development snags:
--     dividing by 8 everywhere is a real pain
--         but is necessary because otherwise rect overlap math
--         is not possible and collision precision
--         and tile occupancy checks are more difficult
--     needed to add an upper limit on how many actors can exist to avoid lag out
--         because logic checks every actor to determine action based on tile
--         alternately could optimize the movement functions for actors
--             to not check all other actors every cycle
--


-----------
-- GLOBALS :(
-----------

actors = {} -- all actors
selectors = {}
structures = {}
counters = {}
default_cursor_sprite = nil
last_click = nil
max_frames = 60
max_actors = 60
cursor_sprite = nil
movement_mode = 'cursor'
player = nil
reference_belt = nil
reference_frame = 0
cursor_data = {
    ['arrow_icon']        = {['first'] = 37, ['last'] = 40},
    ['arrow']             = {['first'] = 53, ['last'] = 56},
    ['building_selector'] = {['first'] = 1 , ['last'] = 7 },
}
arrow_directions = {
    [53] = 'up',
    [54] = 'right',
    [55] = 'down',
    [56] = 'left',
}
production_map = {
    [1] = {
        ['name'] = 'forester',
        ['consumes'] = nil,
        ['produces'] = 'log',
    },
    [2] = {
        ['name'] = 'sawyer',
        ['consumes'] = {
            ['log'] = 1,
        },
        ['produces'] = 'plank',
    },
    [3] = {
        ['name'] = 'woodcutter',
        ['consumes'] = {
            ['log'] = 1,
        },
        ['produces'] = 'firewood',
    },
    [4] = {
        ['name'] = 'coalier',
        ['consumes'] = {
            ['log'] = 1,
        },
        ['produces'] = 'charcoal',
    },
    [5] = {
        ['name'] = 'miner',
        ['consumes'] = nil,
        ['produces'] = 'gold',
    },
    [6] = {
        ['name'] = 'goldsmith',
        ['consumes'] = {
            ['gold'] = 1,
        },
        ['produces'] = 'key',
    },
    [7] = {
        ['name'] = 'storage',
        ['consumes'] = {
            ['log']      = 1,
            ['plank']    = 1,
            ['firewood'] = 1,
            ['charcoal'] = 1,
            ['gold']     = 1,
            ['key']      = 1,
        },
    },
}
production_sprite_map = {
    ['log']      = 80,
    ['plank']    = 81,
    ['firewood'] = 82,
    ['charcoal'] = 83,
    ['gold']     = 84,
    ['key']      = 85,
}

belt_data = {
    --{1,1,1,5},
    --{1,15,6,6},
    {8,9,7,7},
    {10,10,7,4},
    --{5,5,5,10},
}

counter_data = {
    ['log'] = {
        ['icon_position']  = {['x'] = 2, ['y'] = 0},
        ['count_position'] = {['x'] = 3, ['y'] = 0},
        ['count']          = 0,
        ['sprite_id']      = 80,
    },
    ['plank'] = {
        ['icon_position']  = {['x'] = 5, ['y'] = 0},
        ['count_position'] = {['x'] = 6, ['y'] = 0},
        ['count']          = 0,
        ['sprite_id']      = 81,
    },
    ['firewood'] = {
        ['icon_position']  = {['x'] = 8, ['y'] = 0},
        ['count_position'] = {['x'] = 9, ['y'] = 0},
        ['count']          = 0,
        ['sprite_id']      = 82,
    },
    ['charcoal'] = {
        ['icon_position']  = {['x'] = 2, ['y'] = 1},
        ['count_position'] = {['x'] = 3, ['y'] = 1},
        ['count']          = 0,
        ['sprite_id']      = 83,
    },
    ['gold'] = {
        ['icon_position']  = {['x'] = 5, ['y'] = 1},
        ['count_position'] = {['x'] = 6, ['y'] = 1},
        ['count']          = 0,
        ['sprite_id']      = 84,
    },
    ['key'] = {
        ['icon_position']  = {['x'] = 8, ['y'] = 1},
        ['count_position'] = {['x'] = 9, ['y'] = 1},
        ['count']          = 0,
        ['sprite_id']      = 85,
    },
}

-- FILTER LISTS
-- used for `set_contains` operation
--   need entries as keys, because {'value'} will resolve to {[1]='value'}
--   and `set_contains` check will return false when it should match
cursor_sprite_names = {['arrow'] = 1, ['delete'] = 1, ['building_selector'] = 1}
no_animation_sprite_names = {['building'] = 1}
player_targets = {['building'] = 1,}
production_structures = {['building'] = 1,}
structure_names = {['belt'] = 1, ['building'] = 1,}


-----------
-- CORE
-----------

function _init()
    -- create sprites
    create_belts()
    create_counters()
    create_selectors()
    default_cursor_sprite = make_actor(41, 0, 0, 'cursor')
    cursor_sprite = default_cursor_sprite

    -- make player
    player = add_actor(44, 7, 7, 'player')
    player.frames = 4
end

function _update()
    remove_stale_product()
    handle_button_presses()
    if movement_mode == 'player' then
        control_player()
    elseif movement_mode == 'cursor' then
        control_cursor()
    end
    foreach(actors, move_actor)
    foreach(structures, handle_production)
    foreach(structures, animate_stationary)
    manage_frame()
end

function remove_stale_product()
    if #actors >= max_actors then
        for i, v in pairs(actors) do
            if set_contains(production_sprite_map, v.name) then
                del(actors, v)
                break
            end
        end
    end
end

function handle_button_presses()
    debug_button_presses()
    if btnp(4) and btnp(5) then
        -- button 4 and 5 together cycles between control modes
        printh('cycling control mode')
        if cursor_sprite.name == 'arrow' then
            cursor_sprite = default_cursor_sprite
            printh('setting default cursor sprite')
        else
            if movement_mode == 'cursor' then
                movement_mode = 'player'
            else
                movement_mode = 'cursor'
            end
            printh('movement mode is ' .. movement_mode)
        end
        return
    end
    if btnp(5) then
        -- button 5 handles selector sprite cycles in cursor mode
        if movement_mode == 'cursor' then
            if cursor_sprite.name == 'arrow' then
                cycle_selection_sprite('arrow')
                cursor_sprite.direction = arrow_directions[cursor_sprite.sprite_id]
            elseif cursor_sprite.name == 'building_selector' then
                cycle_selection_sprite('building_selector')
            end
        end
        return
    end
    if btnp(4) then
        -- button 4 handles placement and interaction with objects
        if movement_mode == 'cursor' then
            handle_cursor_target()
        else
            handle_player_target()
        end
        return
    end
end

function cycle_selection_sprite(name)
    first = cursor_data[name].first
    last  = cursor_data[name].last
    cursor_sprite.sprite_id += 1
    if cursor_sprite.sprite_id > last then
        cursor_sprite.sprite_id = first
    end
end

function debug_button_presses()
    if (btnp(0)) printh('button 0 pressed')
    if (btnp(1)) printh('button 1 pressed')
    if (btnp(2)) printh('button 2 pressed')
    if (btnp(3)) printh('button 3 pressed')
    if (btnp(4)) printh('button 4 pressed')
    if (btnp(5)) printh('button 5 pressed')
    if (btnp(6)) printh('button 6 pressed')
    if (btnp(7)) printh('button 7 pressed')
    if (btnp(8)) printh('button 8 pressed')
end

function _draw()
    cls()

    map(0, 0, 0, 0, 16, 16)

    handle_debug()

    handle_camera()

    foreach(counters, draw_actor)
    foreach(structures, draw_actor)
    foreach(selectors, draw_actor)
    foreach(actors, draw_actor)
    draw_counters()
    if cursor_sprite then
        draw_actor(cursor_sprite)
    end
end

function handle_debug()
    print(flr(cursor_sprite.x / 8) .. ',' .. flr(cursor_sprite.y / 8), 13*8, 0)
    print(movement_mode, 12*8, 1*8)
end

function draw_counters()
    for i, v in pairs(counter_data) do
        print(v.count, v.count_position.x * 8, v.count_position.y * 8)
    end
end


-----------
-- BUILDINGS
-----------

-- make building selectors
function create_selectors()
    -- add delete selectors
    delete_sprite = make_actor(9, 0, 5, 'delete')
    add(selectors, delete_sprite)

    -- add building selectors
    building_first = cursor_data.building_selector.first
    building_last = cursor_data.building_selector.last
    create_stationary(
        building_first, building_last,
        0, 0, 0, 8,
        selectors,
        'building_selector'
    )
    -- add arrow selectors
    create_arrows()
end

function create_stationary(start_id, end_id, x_start, y_start, x_offset, y_offset, collection, name)
    for j = start_id, end_id do
        x = x_start + 1 * x_offset
        y = y_start + j - start_id + 1 * y_offset
        create_actor_in_collection(j, x, y, collection, name)
    end
end

function create_counters()
    for i, v in pairs(counter_data) do
        create_stationary(
            v.sprite_id,
            v.sprite_id,
            v.icon_position.x,
            v.icon_position.y,
            0,
            0,
            counters,
            i
        )
    end
end

function create_actor_in_collection(sprite_id, x, y, collection, name)
    printh('creating actor ' .. name .. ' at ' .. x .. ',' .. y)
    b = make_actor(sprite_id, x, y, name)
    if set_contains(no_animation_sprite_names, name) then
        b.frames = 1
    end
    add(collection, b)
    return b
end

function handle_production(structure)
    -- check for material in input tile
        -- if found, and if need input, consume
    -- if can produce, check for product in output tile
        -- output tile will always be to right of structure, can change later
        -- if found, do nothing
        -- if not found, make one
    -- reference_frame == 0 will cause this to trigger every 60 frames (2 seconds)
    if not set_contains(production_structures, structure.name) or reference_frame != 0 then
        return
    end

    mapping = production_map[structure.sprite_id]
    consumes = mapping.consumes
    produces = mapping.produces or ''
    printh('handling production for ' .. structure.name .. ' ' .. mapping.name .. ': ' .. produces)
    material = true
    if consumes then
        material = check_for_material(structure, consumes)
    end
    if material then
        if produces == '' then
            -- then this is a counter/storage
            counter_data[material.name].count += 1
            printh('collected ' .. counter_data[material.name].count .. ' ' .. material.name)
        else
            produced = produce(structure, produces)
        end
        if produced or produces == '' then
            del(actors, material)
        end
    else
        printh('no input materal found for ' .. produces .. ' production')
    end
end

function check_for_material(structure, consumes)
    -- check for input requirement on structure
    input_contents = tile_occupied(structure.x, structure.y, actors)
    found_material = nil
    if input_contents and set_contains(consumes, input_contents.name) then
        found_material = input_contents
    end
    return found_material
end

function produce(structure, produces)
    -- check if tile occupied to right of structure
    occupied = tile_occupied(structure.x + 8, structure.y, actors)
    produced = false
    if not occupied and #actors < max_actors then
        product = make_actor(production_sprite_map[produces], flr(structure.x / 8) + 1, flr(structure.y / 8), produces)
        product.frames = 1
        add(actors, product)
        produced = true
        show_table_length(actors)
    end
    if produced then
        printh('produced a ' .. produces .. ' at ' .. structure.x .. ' ' .. structure.y)
    else
        printh('not producing a ' .. produces .. ' at ' .. structure.x .. ' ' .. structure.y)
    end
    return produced
end

-----------
-- ARROWS
-----------

function create_arrows()
    start_id = cursor_data.arrow_icon.first
    end_id   = cursor_data.arrow_icon.last
    for j=start_id,end_id do
        actor = create_actor_in_collection(j, 0, j - start_id, selectors, 'arrow')
        -- arrows menu icon is 16 sprite ids offset from cursor icon
        actor.direction = arrow_directions[j + 16]
    end
end

function arrow_exists(x, y)
    -- this is the same code as the tile_occupied function
    for i, v in pairs(selectors) do
        if flr(v.x) == flr(x) and flr(v.y) == flr(y) then
            return v
        end
    end
    return nil
end


-----------
-- ACTORS
-----------

function add_actor(sprite_id, x, y, name)
    a = make_actor(sprite_id, x, y, name)
    add(actors,a)
    return a
end

-- make an actor
-- and add to global collection
-- x,y means center of the actor
-- in map tiles
function make_actor(sprite_id, x, y, name)
    a={
        sprite_id = sprite_id,
        x = x * 8,
        y = y * 8,
        name = name,
        dx = 0,
        dy = 0,
        frame = 0,
        t = 0,
        friction = 0.35,
        bounce = 0.3,
        direction_x = 1,
        direction_y = 1,
        frames = 2,
        stationary = false,

        -- half-width and half-height
        -- slightly less than 0.5 so
        -- that will fit through 1-wide
        -- holes.
        w = 0.4,
        h = 0.4
    }

    return a
end

function move_actor(a)
    -- on a belt?
    belt_dir_x, belt_dir_y = get_belt_effect(a)

    -- calculate new movement speed
    to_move_x = a.dx + belt_dir_x
    to_move_y = a.dy + belt_dir_y

    -- add movement speed to position
    a.x += to_move_x * 8
    a.y += to_move_y * 8

    -- handle animation speed
    frame_move_default = 0.05
    frame_move_x = frame_move_default
    frame_move_y = frame_move_default
    -- if there's any input, vary animation with that
    if (abs(a.dx) != 0) frame_move_x = abs(to_move_x) * 2
    if (abs(a.dy) != 0) frame_move_y = abs(to_move_y) * 2
    -- set to default if too low
    if (frame_move_x < frame_move_default) frame_move_x = frame_move_default
    if (frame_move_y < frame_move_default) frame_move_y = frame_move_default

    -- adjust the frame
    -- only need to adjust based on fastest direction
    -- or diagonals will double animation speed
    if frame_move_x > frame_move_y then
        a.frame += frame_move_x
    else
        a.frame += frame_move_y
    end
    -- sets equal to remainder
    a.frame %= a.frames

    a.t += 1
    -- apply friction
    -- (comment for no inertia)
    a.dx *= (1-a.friction)
    a.dy *= (1-a.friction)
end

function get_belt_effect(actor)
    belt = on_belt(flr(actor.x), flr(actor.y))
    direction = 'none'
    belt_dir_x = 0
    belt_dir_y = 0
    x_move = 0
    y_move = 0
    if belt then
        belt_speed = 0.05
        direction = get_belt_direction(belt)
        if (direction == 'up'   ) belt_dir_y = -belt_speed y_move = -1
        if (direction == 'left' ) belt_dir_x = -belt_speed x_move = -1
        if (direction == 'down' ) belt_dir_y =  belt_speed y_move =  1
        if (direction == 'right') belt_dir_x =  belt_speed x_move =  1
    end
    next_tile_occupied = tile_occupied(actor.x + x_move, actor.y + y_move, actors, actor)
    --printh(x + x_move .. ',' .. y + y_move)
    if next_tile_occupied then
        belt_dir_x = 0
        belt_dir_y = 0
    end
    return belt_dir_x, belt_dir_y
end

function draw_actor(a)
    local sx = (a.x)
    local sy = (a.y)
    spr(a.sprite_id + a.frame, sx, sy, 1, 1, a.direction_x==-1, a.direction_y==-1)
end


-----------
-- BELTS
-----------

-- make_belts
function create_belts()
    -- make first belt off screen for frame count reference to sync animation
    make_belt(999, 999, 999, 999)
    for i, v in pairs(belt_data) do
        -- offset by half because positions are centers
        make_belt(
            v[1],
            v[2],
            v[3],
            v[4]
        )
    end
end

function make_belt(x_start, x_end, y_start, y_end, options)
    belt = {
        x_start = x_start,
        x_end = x_end,
        y_start = y_start,
        y_end = y_end,
        flip_x = 1,
        flip_y = 1,
        sprite_id = 12
    }
    -- up
    if (options and options.direction == 'up') or y_start > y_end then
        belt.y_start = y_end
        belt.y_end = y_start
    end
    -- left
    if (options and options.direction == 'left') or x_start > x_end then
        belt.flip_x    = -1
        belt.sprite_id = 28
        belt.x_start   = x_end
        belt.x_end     = x_start
    end
    -- down
    if (options and options.direction == 'down') or y_start < y_end then
        belt.flip_y =- 1
    end
    -- right
    if (options and options.direction == 'right') or x_start < x_end then
        belt.sprite_id = 28
    end
    for i=belt.x_start,belt.x_end do
        for j=belt.y_start,belt.y_end do
            a = make_actor(belt.sprite_id, i, j, 'belt')
            a.frames = 4
            a.stationary = true
            a.direction_x = belt.flip_x
            a.direction_y = belt.flip_y
            add(structures, a)
            if reference_belt == nil then
                reference_belt = a
            else
                a.frame = reference_belt.frame
            end
        end
    end
    return belt
end

function on_belt(x, y)
    exists = tile_occupied(x, y, structures)
    if exists and exists.name == 'belt' then
        return exists
    else
        return nil
    end
end

function tile_occupied(x, y, collection, actor)
    offset = 7

    for i, v in pairs(collection) do
        if not (
            -- v left past target's right, or
            -- v right past target's left
            v.x          > x + offset or
            v.x + offset < x          or
            -- v top past target's bottom, or
            -- v bottom past target's top
            v.y          > y + offset or
            v.y + offset < y
        ) and v != actor then
            return v
        end
   end
   return nil
end

function orient_belt(belt)
    new_direction = cursor_sprite.direction
    set_belt_direction(new_direction, belt)
end

function replace_building(building)
    new_sprite_id = cursor_sprite.sprite_id
    building.sprite_id = new_sprite_id
end

function get_belt_direction(belt)
    current_dir = 'up'
    -- 28 is the horizontal sprite
    -- use vars for these sprite ids, in case they move on the sheet
    if (belt.sprite_id == 28 and belt.direction_x == -1) current_dir = 'left'
    if (belt.sprite_id == 28 and belt.direction_x ==  1) current_dir = 'right'
    if (belt.sprite_id == 12 and belt.direction_y == -1) current_dir = 'down'
    return current_dir
end

function set_belt_direction(direction, belt)
    -- defaults to up
    belt.direction_x = 1
    belt.direction_y = 1
    belt.sprite_id = 12
    if direction == 'down' then
        belt.direction_y = -1
    elseif direction == 'left' then
        belt.sprite_id = 28
        belt.direction_x = -1
    elseif direction == 'right' then
        belt.sprite_id = 28
    end
end

function check_for_structure(x, y)
    structure = tile_occupied(x, y, structures)
    if structure then
        return structure
    else
        -- what
        arrow = arrow_exists(x, y)
        return arrow
    end
    return nil
end


-----------
-- CONTROLS
-----------

-- move_player
function control_player()
    accel = 0.05
    if (btn(0)) player.dx -= accel player.direction_x=-1
    if (btn(1)) player.dx += accel player.direction_x=1
    if (btn(2)) player.dy -= accel
    if (btn(3)) player.dy += accel
    -- prevent moving player off screen in x direction
    if (player.x > 15 * 8) player.x = 15 * 8
    if (player.x <  0) player.x = 0
    -- prevent moving player off screen in y direction
    if (player.y > 15 * 8) player.y = 15 * 8
    if (player.y <  0) player.y = 0
end

function control_cursor()
    -- only do this once every 4 frames to avoid skipping too fast to see
    if reference_frame % 4 != 0 then
        return
    end
    if (btn(0)) cursor_sprite.x += -8
    if (btn(1)) cursor_sprite.x +=  8
    if (btn(2)) cursor_sprite.y += -8
    if (btn(3)) cursor_sprite.y +=  8

    -- prevent drawing cursor off screen in x direction
    if (cursor_sprite.x > 15 * 8) cursor_sprite.x = 15 * 8
    if (cursor_sprite.x <  0) cursor_sprite.x = 0
    -- prevent drawing cursor off screen in y direction
    if (cursor_sprite.y > 15 * 8) cursor_sprite.y = 15 * 8
    if (cursor_sprite.y <  0) cursor_sprite.y = 0
end

function handle_player_target()
    x = player.x
    y = player.y
    target_sprite = check_for_structure(x, y)
    if target_sprite and set_contains(player_targets, target_sprite.name) then
        printh('handling player target ' .. target_sprite.name)
    end
end

function handle_cursor_target()
    printh('cursor is at ' .. cursor_sprite.x .. ' ' .. cursor_sprite.y)
    x = cursor_sprite.x
    y = cursor_sprite.y
    target_sprite = check_for_structure(x, y)
    -- handle clicked sprite
    -- this is for changing the target sprite
    if cursor_sprite.name == 'arrow' then
        -- handle conveyor orientation operation
        if target_sprite == nil then
            options = {}
            options.direction = cursor_sprite.direction
            make_belt(x / 8, x / 8, y / 8, y / 8, options)
        elseif target_sprite.name == 'belt' then
            orient_belt(target_sprite)
        end
    elseif cursor_sprite.name == 'delete' then
        -- handle target delete operation
        if target_sprite and set_contains(structure_names, target_sprite.name) then
            del(structures, target_sprite)
        end
    elseif cursor_sprite.name == 'building_selector' then
        -- handle building selector operation
        if target_sprite == nil then
            sprite_id = cursor_sprite.sprite_id
            create_actor_in_collection(sprite_id, x / 8, y / 8, structures, 'building')
        elseif target_sprite.name == 'building' then
            replace_building(target_sprite)
        end
    end

    -- assign target sprite to cursor
    -- this is for changing the cursor selection only
    if target_sprite != nil then
        printh(target_sprite.name)
        printh(cursor_sprite_names)
        sprite_id_offset = 16
        if set_contains(cursor_sprite_names, target_sprite.name) then
        -- if target_sprite.name == 'arrow' or target_sprite.name == 'delete' or target_sprite.name == 'building_selector' then
            if target_sprite.name == 'building_selector' then
                sprite_id_offset = 0
            end
            cursor_sprite = copy_sprite(target_sprite, tonum(target_sprite.sprite_id) + sprite_id_offset)
        end
    end
end

function set_contains(set, key)
    return set[key] ~= nil
end

function handle_camera()
    camera(0, 0)
    -- enabling these lines will allow camera to follow player off current screen
    -- room_x = flr(player.x / 16)
    -- room_y = flr(player.y / 16)
    -- camera(room_x * 128, room_y * 128)
end


-----------
-- UTILITIES
-----------

function copy_sprite(sprite, new_id, new_name)
    new_sprite = make_actor(sprite.sprite_id, sprite.x / 8, sprite.y / 8, sprite.name)
    if (new_id)    new_sprite.sprite_id = new_id
    if (name)      new_sprite.name = new_name
    if (direction) new_sprite.direction = sprite.direction
    return new_sprite
end

function animate_stationary(a)
    -- why 0.25? quarter of a square
    a.frame += 0.25
    a.frame %= a.frames
end

function manage_frame()
    reference_frame += 1
    reference_frame %= max_frames
end

function show_table_length(T)
    printh('table elements: ' .. #T)
end
