function getnext(start,coll,rev)
    local starti,endi,iter,get_next,result=rev and #coll or 1,rev and 1 or #coll,rev and -1 or 1,nil,nil
    for i=starti,endi,iter do
        if(result!=nil)break
        if(get_next)result=coll[i]
        get_next=tostr(start)==tostr(coll[i])
    end
    if(result!=nil)return result else return coll[starti]
end

function contains(set,key) return set~=nil and set[key]~=nil end
function prpos(x,y) return x..','..y end
function getvec(a) return a.x,a.y end
function contains_attr(set,attr,val) for i,v in pairs(set) do if (v[attr]==val) return i,v or -1,{} end end
function pythag(a,b) return sqrt(a^2+b^2) end
function sort(a,fld,isint)
    for i=1,#a do
        local j=i
        while j>1 do
            local fld1,fld2=a[j-1][fld],a[j][fld]
            if(isint)fld1,fld2=tonum(fld1),tonum(fld2)
            if fld1>fld2 then
                local tomove=a[j]
                del(a,tomove)
                add(a,tomove,j-1)
            end
            j=j-2
        end
    end
end
function make_obj(o,self) setmetatable(o, self) self.__index=self end
function chktm(tm) return abs(tm-max(time(),tm)) end
function search(coll,st)
    for ea in all(coll) do if st==ea then return true end end return false
end
function spl_crds(dta)
    local out={}
    for i,v in pairs(dta) do
        if i%2!=0 then
            add(out,{x=v,y=dta[i+1]})
        end
    end
    return out
end
function chst_snd(a) if(a.on) sfx(52) else sfx(53) end
function setm(a,id) mset(a.x,a.y,id or a.sprite_id) end
function set_ix(dta,cur)
    local ret=cur
    for i,v in pairs(dta) do
        v.ix=i
        if(cur.cmpl)ret=v v.selected=true
    end
    return ret
end
function rmap(dt)
    for ea in all(split(dt,'|')) do
        local s=split(ea)
        local x,y,sp=s[1],s[2],s[3]
        for ea in all(wave_cells) do
            if(ea.x==x and ea.y==y)del(wave_cells,ea)
        end
        mset(x,y,sp)
    end
end
function rrng(n,o) return rnd(n)+o end
function set_tgt_dir(a)
    local spd=a.spd or 0.1
    if(a.x<a.xt)a.dx+=spd a.flip_x=false
    if(a.x>a.xt)a.dx+=-spd a.flip_x=true
    if(a.y<a.yt)a.dy+=spd
    if(a.y>a.yt)a.dy+=-spd
end

function sw_msc(n)
    if(trk!=n)trk=n music(trk,0,8)
end
